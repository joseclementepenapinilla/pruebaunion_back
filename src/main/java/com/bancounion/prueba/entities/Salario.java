package com.bancounion.prueba.entities;

import java.util.Date;

import com.bancounion.prueba.entities.embedded.SalarioKey;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "CAO_SALARIO")
@IdClass(SalarioKey.class)
public class Salario {

	@Id
	@JoinColumn(name = "co_usuario")
	@OneToOne(fetch = FetchType.LAZY)
	private Usuario usuario;

	@Id
	@Column(name ="dt_alteracao")
	private Date dt_alteracao;

	@Column(name = "brut_salario")
	private Float  brut_salario;

	@Column(name = "liq_salario")
	private Float  liq_salario;

	public Salario(Usuario usuario, Date fecha_modificacion, Float  salario_bruto, Float  salario_liquido) {
		super();
		this.usuario = usuario;
		this.dt_alteracao = fecha_modificacion;
		this.brut_salario = salario_bruto;
		this.liq_salario = salario_liquido;
	}

	public Salario() {
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getDt_alteracao() {
		return dt_alteracao;
	}

	public void setDt_alteracao(Date dt_alteracao) {
		this.dt_alteracao = dt_alteracao;
	}

	public Float getBrut_salario() {
		return brut_salario;
	}

	public void setBrut_salario(Float brut_salario) {
		this.brut_salario = brut_salario;
	}

	public Float getLiq_salario() {
		return liq_salario;
	}

	public void setLiq_salario(Float salarioLiquido) {
		this.liq_salario = salarioLiquido;
	}

}
