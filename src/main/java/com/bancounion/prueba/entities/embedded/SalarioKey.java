package com.bancounion.prueba.entities.embedded;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalarioKey implements Serializable{

	/**
	 * 
	 */
	private static final Long serialVersionUID = 1L;

	private String usuario;
	
	private Date dt_alteracao;

	public SalarioKey() {}
	
	public SalarioKey(String usuario, Date fechaModificacion) {
		super();
		this.usuario = usuario;
		this.dt_alteracao = fechaModificacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFechaModificacion() {
		return dt_alteracao;
	}

	public void setFechaModificacion(Date fecha_modificacion) {
		this.dt_alteracao = fecha_modificacion;
	}

	public static Long getSerialversionuid() {
		return serialVersionUID;
	}

}
