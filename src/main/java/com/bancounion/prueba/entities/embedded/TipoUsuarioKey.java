package com.bancounion.prueba.entities.embedded;

import java.io.Serializable;

import jakarta.persistence.Column;

public class TipoUsuarioKey implements Serializable{

	/**
	 * 
	 */
	private static final Long serialVersionUID = 1L;

	private Integer coTipoUsuario;
	
	private Integer sistema;
}
