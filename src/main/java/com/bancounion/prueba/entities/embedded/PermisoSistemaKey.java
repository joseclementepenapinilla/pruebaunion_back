package com.bancounion.prueba.entities.embedded;

import java.io.Serializable;

import com.bancounion.prueba.entities.Usuario;

import jakarta.persistence.*;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
public class PermisoSistemaKey implements Serializable{

	  /**
	 * 
	 */
	private static final Long serialVersionUID = 1L;
 
	  private Usuario coUsuario;

	  private Integer coTipoUsuario;

	  private Integer coSistema;
	  	  
}
