package com.bancounion.prueba.entities;

import java.util.Date;
import java.util.List;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity(name="CAO_OS")
public class OrdenServicio {
	
	@Id
    @Column(name ="co_os")
	private Integer co_os;
	
	@Column(name ="nu_os")
	private Integer nu_os;
	
	@Column(name ="co_sistema")
	private Integer co_sistema;
	
	@ManyToOne
	@JoinColumn(name ="co_usuario")
	private Usuario usuario;
	
	@Column(name ="co_arquitetura")
	private Integer co_arquitectura;
	
	@Column(name ="ds_os")
	private String ds_os;
	
	@Column(name ="ds_caracteristica")
	private String caracteristica;
	
	@Column(name ="ds_requisito")
	private String requisito;
	
	@Column(name ="dt_inicio")
	private Date fecha_inicio;
	
	@Column(name ="dt_fim")
	private Date fecha_fin;
	
	@Column(name ="co_status")
	private Integer status;
	
	@Column(name ="diretoria_sol")
	private String directorio;
	
	@Column(name ="dt_sol")
	private Date fecha_sol;
	
	@Column(name ="nu_tel_sol")
	private String nu_tel_sol;
	
	@Column(name ="ddd_tel_sol")
	private String ddd_tel_sol;
	
	@Column(name ="nu_tel_sol2")
	private String nu_tel_sol_dos;
	
	@Column(name ="ddd_tel_sol2")
	private String ddd_tel_sol_dos;
	
	@Column(name ="usuario_sol")
	private String usuario_sol;
	
	@Column(name ="dt_imp")
	private Date fecha_imp;
	
	@Column(name ="dt_garantia")
	private Date fecha_garantia;
	
	//Corregir tipo de dato si hace refereia a un correo electronico
	@Column(name ="co_email")
	private String correo_electronico;
	
	@Column(name ="co_os_prospect_rel")
	private String prospect_rel;

	@OneToMany(mappedBy ="ordenServicio", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Factura> facturas;
	
	public OrdenServicio() {}
	
	public OrdenServicio(Integer co_os, Integer nu_os, Integer idSistema, Usuario usuario, Integer co_arquitectura,
			String ds_os, String caracteristica, String requisito, Date fecha_inicio, Date fecha_fin, Integer status,
			String directorio, Date fecha_sol, String nu_tel_sol, String ddd_tel_sol, String nu_tel_sol_dos,
			String ddd_tel_sol_dos, String usuario_sol, Date fecha_imp, Date fecha_garantia, String correo_electronico,
			String prospect_rel, List<Factura> facturas) {
		super();
		this.co_os = co_os;
		this.nu_os = nu_os;
		this.co_sistema = idSistema;
		this.usuario = usuario;
		this.co_arquitectura = co_arquitectura;
		this.ds_os = ds_os;
		this.caracteristica = caracteristica;
		this.requisito = requisito;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.status = status;
		this.directorio = directorio;
		this.fecha_sol = fecha_sol;
		this.nu_tel_sol = nu_tel_sol;
		this.ddd_tel_sol = ddd_tel_sol;
		this.nu_tel_sol_dos = nu_tel_sol_dos;
		this.ddd_tel_sol_dos = ddd_tel_sol_dos;
		this.usuario_sol = usuario_sol;
		this.fecha_imp = fecha_imp;
		this.fecha_garantia = fecha_garantia;
		this.correo_electronico = correo_electronico;
		this.prospect_rel = prospect_rel;
		this.facturas = facturas;
	}

	public Integer getCo_os() {
		return co_os;
	}

	public void setCo_os(Integer co_os) {
		this.co_os = co_os;
	}

	public Integer getNu_os() {
		return nu_os;
	}

	public void setNu_os(Integer nu_os) {
		this.nu_os = nu_os;
	}

	public Integer getCo_sistema() {
		return co_sistema;
	}

	public void setCo_sistema(Integer idSistema) {
		this.co_sistema = idSistema;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getCo_arquitectura() {
		return co_arquitectura;
	}

	public void setCo_arquitectura(Integer co_arquitectura) {
		this.co_arquitectura = co_arquitectura;
	}

	public String getDs_os() {
		return ds_os;
	}

	public void setDs_os(String ds_os) {
		this.ds_os = ds_os;
	}

	public String getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}

	public String getRequisito() {
		return requisito;
	}

	public void setRequisito(String requisito) {
		this.requisito = requisito;
	}

	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

	public Date getFecha_sol() {
		return fecha_sol;
	}

	public void setFecha_sol(Date fecha_sol) {
		this.fecha_sol = fecha_sol;
	}

	public String getNu_tel_sol() {
		return nu_tel_sol;
	}

	public void setNu_tel_sol(String nu_tel_sol) {
		this.nu_tel_sol = nu_tel_sol;
	}

	public String getDdd_tel_sol() {
		return ddd_tel_sol;
	}

	public void setDdd_tel_sol(String ddd_tel_sol) {
		this.ddd_tel_sol = ddd_tel_sol;
	}

	public String getNu_tel_sol_dos() {
		return nu_tel_sol_dos;
	}

	public void setNu_tel_sol_dos(String nu_tel_sol_dos) {
		this.nu_tel_sol_dos = nu_tel_sol_dos;
	}

	public String getDdd_tel_sol_dos() {
		return ddd_tel_sol_dos;
	}

	public void setDdd_tel_sol_dos(String ddd_tel_sol_dos) {
		this.ddd_tel_sol_dos = ddd_tel_sol_dos;
	}

	public String getUsuario_sol() {
		return usuario_sol;
	}

	public void setUsuario_sol(String usuario_sol) {
		this.usuario_sol = usuario_sol;
	}

	public Date getFecha_imp() {
		return fecha_imp;
	}

	public void setFecha_imp(Date fecha_imp) {
		this.fecha_imp = fecha_imp;
	}

	public Date getFecha_garantia() {
		return fecha_garantia;
	}

	public void setFecha_garantia(Date fecha_garantia) {
		this.fecha_garantia = fecha_garantia;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public String getProspect_rel() {
		return prospect_rel;
	}

	public void setProspect_rel(String prospect_rel) {
		this.prospect_rel = prospect_rel;
	}

	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}
		
}
