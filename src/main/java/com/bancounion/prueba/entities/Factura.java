package com.bancounion.prueba.entities;

import java.util.Date;

import jakarta.persistence.*;
import lombok.*;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name ="CAO_FATURA")
public class Factura {
	
	@Id
	@Column(name = "co_fatura")
	private Integer factura;
	
	@Column(name ="co_cliente")
	private Integer cliente;
	
	@Column(name ="co_sistema")
	private Integer sistema;

	@ManyToOne()
	@JoinColumn(name ="co_os")
	private OrdenServicio ordenServicio;
	
	@Column(name ="num_nf")
	private Integer num_nf;
	
	@Column(name ="total")
	private float total;
	
	@Column(name ="valor")
	private float valor;
	
	@Column(name ="data_emissao")
	private Date fecha_emision;
	
	@Column(name ="corpo_nf")
	private String corpo_nf;
	
	@Column(name ="comissao_cn")
	private float comision;
	
	@Column(name ="total_imp_inc")
	private float tota_imp_inc;
	
	public Factura() {}
	public Factura(Integer factura, Integer cliente, Integer sistema, OrdenServicio os, Integer num_nf, float total, float valor,
			Date fecha_emision, String corpo_nf, float comision, float tota_imp_inc) {
		super();
		this.factura = factura;
		this.cliente = cliente;
		this.sistema = sistema;
		this.ordenServicio = os;
		this.num_nf = num_nf;
		this.total = total;
		this.valor = valor;
		this.fecha_emision = fecha_emision;
		this.corpo_nf = corpo_nf;
		this.comision = comision;
		this.tota_imp_inc = tota_imp_inc;
	}
	public Integer getFactura() {
		return factura;
	}
	public void setFactura(Integer factura) {
		this.factura = factura;
	}
	public Integer getCliente() {
		return cliente;
	}
	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}
	public Integer getSistema() {
		return sistema;
	}
	public void setSistema(Integer sistema) {
		this.sistema = sistema;
	}
	public OrdenServicio getOrdenServicio() {
		return ordenServicio;
	}
	public void setOrdenServicio(OrdenServicio orden_servicio) {
		this.ordenServicio = orden_servicio;
	}
	public Integer getNum_nf() {
		return num_nf;
	}
	public void setNum_nf(Integer num_nf) {
		this.num_nf = num_nf;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public Date getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(Date fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	public String getCorpo_nf() {
		return corpo_nf;
	}
	public void setCorpo_nf(String corpo_nf) {
		this.corpo_nf = corpo_nf;
	}
	public float getComision() {
		return comision;
	}
	public void setComision(float comision) {
		this.comision = comision;
	}
	public float getTota_imp_inc() {
		return tota_imp_inc;
	}
	public void setTota_imp_inc(float tota_imp_inc) {
		this.tota_imp_inc = tota_imp_inc;
	}
	
}
