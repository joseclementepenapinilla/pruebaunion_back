package com.bancounion.prueba.entities;

import java.util.List;

import jakarta.persistence.*;
import lombok.*;

@Getter @Setter @NoArgsConstructor
@Entity(name ="CAO_CLIENTE")
public class Cliente {
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name ="co_cliente")
	private Integer co_cliente;
	
	@Column(name ="no_razao")
	private String no_razao;
	
	@Column(name ="no_fantasia")
	private String no_fantasia;
	
	@Column(name ="no_contato")
	private String no_contrato;
	
	@Column(name ="nu_telefone")
	private String nro_telefono;
	
	@Column(name ="nu_ramal")
	private String ramal;
	
	@Column(name ="nu_cnpj")
	private String cnpj;
	
	@Column(name ="ds_endereco")
	private String entereco;
		
	@Column(name ="nu_numero")
	private Integer numero;
	
	@Column(name ="ds_complemento")
	private String complemento;
	
	@Column(name ="no_bairro")
	private String bairro;
	
	@Column(name ="nu_cep")
	private String nu_cep;
	
	@Column(name ="no_pais")
	private String pais;
	
	@Column(name ="co_ramo")
	private Integer ramo;
	
	@Column(name ="co_cidade")
	private Integer cidade;
	
	@Column(name ="co_status")
	private Integer status;
	
	@Column(name ="ds_site")
	private String site;
	
	@Column(name ="ds_email")
	private String correo_electronico;
	
	@Column(name ="ds_cargo_contato")
	private String cargo;
	
	@Column(name ="tp_cliente")
	private String cliente;
	
	@Column(name ="ds_referencia")
	private String referencia;
	
	@Column(name ="co_complemento_status")
	private Integer complemento_status;
	
	@Column(name ="nu_fax")
	private String fax;
	
	@Column(name ="ddd2")
	private String ddd;
	
	@Column(name ="telefone2")
	private String telefono;

	@OneToMany(mappedBy ="cliente", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Factura> facturas;

	public Cliente() {}
	
	public Cliente(Integer id_cliente, String no_razao, String no_fantasia, String no_contrato, String nro_telefono,
			String ramal, String cnpj, String entereco, Integer numero, String complemento, String bairro, String nu_cep,
			String pais, Integer ramo, Integer cidade, Integer status, String site, String correo_electronico, String cargo,
			String cliente, String referencia, Integer complemento_status, String fax, String ddd, String telefono,
			List<Factura> facturas) {
		super();
		this.co_cliente = id_cliente;
		this.no_razao = no_razao;
		this.no_fantasia = no_fantasia;
		this.no_contrato = no_contrato;
		this.nro_telefono = nro_telefono;
		this.ramal = ramal;
		this.cnpj = cnpj;
		this.entereco = entereco;
		this.numero = numero;
		this.complemento = complemento;
		this.bairro = bairro;
		this.nu_cep = nu_cep;
		this.pais = pais;
		this.ramo = ramo;
		this.cidade = cidade;
		this.status = status;
		this.site = site;
		this.correo_electronico = correo_electronico;
		this.cargo = cargo;
		this.cliente = cliente;
		this.referencia = referencia;
		this.complemento_status = complemento_status;
		this.fax = fax;
		this.ddd = ddd;
		this.telefono = telefono;
		this.facturas = facturas;
	}

	public Integer getId_cliente() {
		return co_cliente;
	}

	public void setId_cliente(Integer id_cliente) {
		this.co_cliente = id_cliente;
	}

	public String getNo_razao() {
		return no_razao;
	}

	public void setNo_razao(String no_razao) {
		this.no_razao = no_razao;
	}

	public String getNo_fantasia() {
		return no_fantasia;
	}

	public void setNo_fantasia(String no_fantasia) {
		this.no_fantasia = no_fantasia;
	}

	public String getNo_contrato() {
		return no_contrato;
	}

	public void setNo_contrato(String no_contrato) {
		this.no_contrato = no_contrato;
	}

	public String getNro_telefono() {
		return nro_telefono;
	}

	public void setNro_telefono(String nro_telefono) {
		this.nro_telefono = nro_telefono;
	}

	public String getRamal() {
		return ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getEntereco() {
		return entereco;
	}

	public void setEntereco(String entereco) {
		this.entereco = entereco;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNu_cep() {
		return nu_cep;
	}

	public void setNu_cep(String nu_cep) {
		this.nu_cep = nu_cep;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public Integer getRamo() {
		return ramo;
	}

	public void setRamo(Integer ramo) {
		this.ramo = ramo;
	}

	public Integer getCidade() {
		return cidade;
	}

	public void setCidade(Integer cidade) {
		this.cidade = cidade;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Integer getComplemento_status() {
		return complemento_status;
	}

	public void setComplemento_status(Integer complemento_status) {
		this.complemento_status = complemento_status;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}
	

}
