package com.bancounion.prueba.entities;

import java.util.Date;
import java.util.List;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@Entity(name ="CAO_USUARIO")
public class Usuario {

	@Id
	@Column(name ="co_usuario")
	private String coUsuario;

	@Column(name ="no_usuario")
	private String no_usuario;

	@Column(name ="ds_senha")
	private String senha;

	@Column(name ="co_usuario_autorizacao")
	private String usuario_autorizado;

	@Column(name ="nu_matricula")
	private Integer nro_matricula;

	@Column(name ="dt_nascimento")
	private Date fecha_nacimiento;

	@Column(name ="dt_admissao_empresa")
	private Date fecha_ingreso_empresa;

	@Column(name ="dt_desligamento")
	private Date fecha_salida_empresa;

	@Column(name ="dt_inclusao")
	private Date fecha_incluido;

	@Column(name ="dt_expiracao")
	private Date fecha_expira;

	@Column(name ="nu_cpf")
	private String nro_cpf;

	@Column(name ="nu_rg")
	private String nro_rg;

	@Column(name ="no_orgao_emissor")
	private String org_emisor;

	@Column(name ="uf_orgao_emissor")
	private String uf_org_emisor;

	@Column(name ="ds_endereco")
	private String ds_endereco;

	@Column(name ="no_email")
	private String correo_electronico;

	@Column(name ="no_email_pessoal")
	private String correo_personal;

	@Column(name ="nu_telefone")
	private String nro_telefono;

	@Column(name ="dt_alteracao")
	private Date fecha_alter;

	@Column(name ="url_foto")
	private String url_foto;

	@Column(name ="instant_messenger")
	private String messenger;

	@Column(name ="icq")
	private Integer icq;

	@Column(name ="msn")
	private String msn;

	@Column(name ="yms")
	private String yms;

	@Column(name ="ds_comp_end")
	private String ds_comp_end;

	@Column(name ="ds_bairro")
	private String barrio;

	@Column(name ="nu_cep")
	private String nu_cep;

	@Column(name ="no_cidade")
	private String no_cidade;

	@Column(name ="uf_cidade")
	private String us_cidate;

	@Column(name ="dt_expedicao")
	private Date fecha_expedicion;

	@OneToMany(mappedBy ="coUsuario")
	private List<PermisoSistema> permisoList;

	@OneToOne(mappedBy = "usuario", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Salario salario;

	@OneToMany(mappedBy="usuario", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<OrdenServicio> Os;
	
	public Usuario() {
	}
	
	public Usuario(String coUsuario, String no_usuario, String senha, String usuario_autorizado, Integer nro_matricula,
			Date fecha_nacimiento, Date fecha_ingreso_empresa, Date fecha_salida_empresa, Date fecha_incluido,
			Date fecha_expira, String nro_cpf, String nro_rg, String org_emisor, String uf_org_emisor,
			String ds_endereco, String correo_electronico, String correo_personal, String nro_telefono,
			Date fecha_alter, String url_foto, String messenger, Integer icq, String msn, String yms,
			String ds_comp_end, String barrio, String nu_cep, String no_cidade, String us_cidate, Date fecha_expedicion,
			List<PermisoSistema> permisoList, Salario salario, List<OrdenServicio> os) {
		super();
		this.coUsuario = coUsuario;
		this.no_usuario = no_usuario;
		this.senha = senha;
		this.usuario_autorizado = usuario_autorizado;
		this.nro_matricula = nro_matricula;
		this.fecha_nacimiento = fecha_nacimiento;
		this.fecha_ingreso_empresa = fecha_ingreso_empresa;
		this.fecha_salida_empresa = fecha_salida_empresa;
		this.fecha_incluido = fecha_incluido;
		this.fecha_expira = fecha_expira;
		this.nro_cpf = nro_cpf;
		this.nro_rg = nro_rg;
		this.org_emisor = org_emisor;
		this.uf_org_emisor = uf_org_emisor;
		this.ds_endereco = ds_endereco;
		this.correo_electronico = correo_electronico;
		this.correo_personal = correo_personal;
		this.nro_telefono = nro_telefono;
		this.fecha_alter = fecha_alter;
		this.url_foto = url_foto;
		this.messenger = messenger;
		this.icq = icq;
		this.msn = msn;
		this.yms = yms;
		this.ds_comp_end = ds_comp_end;
		this.barrio = barrio;
		this.nu_cep = nu_cep;
		this.no_cidade = no_cidade;
		this.us_cidate = us_cidate;
		this.fecha_expedicion = fecha_expedicion;
		this.permisoList = permisoList;
		this.salario = salario;
		Os = os;
	}

	public String getCoUsuario() {
		return coUsuario;
	}
	public void setCoUsuario(String coUsuario) {
		this.coUsuario = coUsuario;
	}
	public String getNo_usuario() {
		return no_usuario;
	}

	public void setNo_usuario(String no_usuario) {
		this.no_usuario = no_usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getUsuario_autorizado() {
		return usuario_autorizado;
	}

	public void setUsuario_autorizado(String usuario_autorizado) {
		this.usuario_autorizado = usuario_autorizado;
	}

	public Integer getNro_matricula() {
		return nro_matricula;
	}

	public void setNro_matricula(Integer nro_matricula) {
		this.nro_matricula = nro_matricula;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public Date getFecha_ingreso_empresa() {
		return fecha_ingreso_empresa;
	}

	public void setFecha_ingreso_empresa(Date fecha_ingreso_empresa) {
		this.fecha_ingreso_empresa = fecha_ingreso_empresa;
	}

	public Date getFecha_salida_empresa() {
		return fecha_salida_empresa;
	}

	public void setFecha_salida_empresa(Date fecha_salida_empresa) {
		this.fecha_salida_empresa = fecha_salida_empresa;
	}

	public Date getFecha_incluido() {
		return fecha_incluido;
	}

	public void setFecha_incluido(Date fecha_incluido) {
		this.fecha_incluido = fecha_incluido;
	}

	public Date getFecha_expira() {
		return fecha_expira;
	}

	public void setFecha_expira(Date fecha_expira) {
		this.fecha_expira = fecha_expira;
	}

	public String getNro_cpf() {
		return nro_cpf;
	}

	public void setNro_cpf(String nro_cpf) {
		this.nro_cpf = nro_cpf;
	}

	public String getNro_rg() {
		return nro_rg;
	}

	public void setNro_rg(String nro_rg) {
		this.nro_rg = nro_rg;
	}

	public String getOrg_emisor() {
		return org_emisor;
	}

	public void setOrg_emisor(String org_emisor) {
		this.org_emisor = org_emisor;
	}

	public String getUf_org_emisor() {
		return uf_org_emisor;
	}

	public void setUf_org_emisor(String uf_org_emisor) {
		this.uf_org_emisor = uf_org_emisor;
	}

	public String getDs_endereco() {
		return ds_endereco;
	}

	public void setDs_endereco(String ds_endereco) {
		this.ds_endereco = ds_endereco;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public String getCorreo_personal() {
		return correo_personal;
	}

	public void setCorreo_personal(String correo_personal) {
		this.correo_personal = correo_personal;
	}

	public String getNro_telefono() {
		return nro_telefono;
	}

	public void setNro_telefono(String nro_telefono) {
		this.nro_telefono = nro_telefono;
	}

	public Date getFecha_alter() {
		return fecha_alter;
	}

	public void setFecha_alter(Date fecha_alter) {
		this.fecha_alter = fecha_alter;
	}

	public String getUrl_foto() {
		return url_foto;
	}

	public void setUrl_foto(String url_foto) {
		this.url_foto = url_foto;
	}

	public String getMessenger() {
		return messenger;
	}

	public void setMessenger(String messenger) {
		this.messenger = messenger;
	}

	public Integer getIcq() {
		return icq;
	}

	public void setIcq(Integer icq) {
		this.icq = icq;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getYms() {
		return yms;
	}

	public void setYms(String yms) {
		this.yms = yms;
	}

	public String getDs_comp_end() {
		return ds_comp_end;
	}

	public void setDs_comp_end(String ds_comp_end) {
		this.ds_comp_end = ds_comp_end;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getNu_cep() {
		return nu_cep;
	}

	public void setNu_cep(String nu_cep) {
		this.nu_cep = nu_cep;
	}

	public String getNo_cidade() {
		return no_cidade;
	}

	public void setNo_cidade(String no_cidade) {
		this.no_cidade = no_cidade;
	}

	public String getUs_cidate() {
		return us_cidate;
	}

	public void setUs_cidate(String us_cidate) {
		this.us_cidate = us_cidate;
	}

	public Date getFecha_expedicion() {
		return fecha_expedicion;
	}

	public void setFecha_expedicion(Date fecha_expedicion) {
		this.fecha_expedicion = fecha_expedicion;
	}

	public List<PermisoSistema> getPermisoList() {
		return permisoList;
	}

	public void setPermisoList(List<PermisoSistema> permisoList) {
		this.permisoList = permisoList;
	}

	public Salario getSalario() {
		return salario;
	}

	public void setSalario(Salario salario) {
		this.salario = salario;
	}

	public List<OrdenServicio> getOs() {
		return Os;
	}
	
	public void setOs(List<OrdenServicio> os) {
		Os = os;
	}

}
