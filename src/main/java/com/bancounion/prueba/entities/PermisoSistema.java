package com.bancounion.prueba.entities;

import java.util.Date;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.bancounion.prueba.entities.embedded.PermisoSistemaKey;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

@Entity(name = "PERMISSAO_SISTEMA")
@IdClass(PermisoSistemaKey.class)
public class PermisoSistema {

	@Id
	@ManyToOne()
	//@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "co_usuario")
	private Usuario coUsuario;

	@Id
	@ManyToOne
	@JoinColumn(name = "co_tipo_usuario")
	private TipoUsuario coTipoUsuario;

	@Id
	@ManyToOne
	@JoinColumn(name = "co_sistema")
	private Sistema coSistema;
	
	@Column(name = "in_ativo")
	private String activo;

	@Column(name = "co_usuario_atualizacao")
	private String coUsuarioActualizado;

	@Column(name = "dt_atualizacao")
	private Date fechaActualizacion;


	public PermisoSistema() {}


	public PermisoSistema(Usuario coUsuario, TipoUsuario coTipoUsuario, Sistema coSistema, String activo,
			String coUsuarioActualizado, Date fechaActualizacion) {
		super();
		this.coUsuario = coUsuario;
		this.coTipoUsuario = coTipoUsuario;
		this.coSistema = coSistema;
		this.activo = activo;
		this.coUsuarioActualizado = coUsuarioActualizado;
		this.fechaActualizacion = fechaActualizacion;
	}


	public Usuario getCoUsuario() {
		return coUsuario;
	}


	public void setCoUsuario(Usuario coUsuario) {
		this.coUsuario = coUsuario;
	}


	public TipoUsuario getCoTipoUsuario() {
		return coTipoUsuario;
	}


	public void setCoTipoUsuario(TipoUsuario coTipoUsuario) {
		this.coTipoUsuario = coTipoUsuario;
	}


	public Sistema getCoSistema() {
		return coSistema;
	}


	public void setCoSistema(Sistema coSistema) {
		this.coSistema = coSistema;
	}


	public String getActivo() {
		return activo;
	}


	public void setActivo(String activo) {
		this.activo = activo;
	}


	public String getCoUsuarioActualizado() {
		return coUsuarioActualizado;
	}


	public void setCoUsuarioActualizado(String coUsuarioActualizado) {
		this.coUsuarioActualizado = coUsuarioActualizado;
	}


	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}


	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	
}
