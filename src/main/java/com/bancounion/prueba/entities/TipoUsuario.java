package com.bancounion.prueba.entities;

import java.util.List;

import com.bancounion.prueba.entities.embedded.TipoUsuarioKey;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.OneToMany;
import lombok.*;

@Entity(name="TIPO_USUARIO" )
@Getter @Setter
@NoArgsConstructor
public class TipoUsuario {

	@Id
	@Column(name="co_tipo_usuario"	)
	private Integer coTipoUsuario;
	
	@Column(name="co_sistema")
	private Integer sistema;
	
	@Column(name="ds_tipo_usuario"	)
	private String descripcion;
	
	@OneToMany(mappedBy ="coTipoUsuario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PermisoSistema> permisosList;

	public Integer getCoTipoUsuario() {
		return coTipoUsuario;
	}

	public void setCoTipoUsuario(Integer coTipoUsuario) {
		this.coTipoUsuario = coTipoUsuario;
	}

	public Integer getSistema() {
		return sistema;
	}

	public void setSistema(Integer sistema) {
		this.sistema = sistema;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<PermisoSistema> getPermisosList() {
		return permisosList;
	}

	public void setPermisosList(List<PermisoSistema> permisosList) {
		this.permisosList = permisosList;
	}

	public TipoUsuario(Integer tipo, Integer sistema, String descripcion, List<PermisoSistema> permisosList) {
		super();
		this.coTipoUsuario = tipo;
		this.sistema = sistema;
		this.descripcion = descripcion;
		this.permisosList = permisosList;
	}

	public TipoUsuario() {
		super();
	}

	
}