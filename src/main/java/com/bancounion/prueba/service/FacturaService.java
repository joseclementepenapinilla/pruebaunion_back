package com.bancounion.prueba.service;

import java.io.FileNotFoundException;
import java.util.List;

import com.bancounion.DTO.FacturaDTO;
import com.bancounion.prueba.entities.Cliente;
import com.bancounion.prueba.entities.Factura;
import com.bancounion.prueba.entities.OrdenServicio;

public interface FacturaService {

	public List<Factura> getFacturaByCLiente(Cliente cl)  throws FileNotFoundException;
	
	public List<Factura> getFacturaByOs(OrdenServicio o) throws FileNotFoundException;
	
	public Object getFacturaByUser(String user) ;
	
	public Object getFacturaForGraph(String user);
}
