package com.bancounion.prueba.service;

import java.util.List;

import com.bancounion.DTO.OrdenServicioDTO;
import com.bancounion.prueba.entities.Usuario;

public interface OsService {

	public List<OrdenServicioDTO> getOsByUsuario(String u);
	
}
