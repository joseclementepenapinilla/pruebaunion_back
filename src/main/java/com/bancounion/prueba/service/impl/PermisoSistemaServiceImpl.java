package com.bancounion.prueba.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancounion.DTO.PermisoSistemaDTO;
import com.bancounion.prueba.entities.*;
import com.bancounion.prueba.repo.PermisoSistemaRepository;
import com.bancounion.prueba.service.PermisoSistemaService;
import com.bancounion.prueba.util.Util;
@Service
public class PermisoSistemaServiceImpl implements PermisoSistemaService {

	@Autowired
	PermisoSistemaRepository repositorio;
	
	@Override
	public List<PermisoSistemaDTO> listarUsuariosCondition(String id, int sistema){
	
		Sistema sis= new Sistema();
		sis.setId(sistema);		
		List<TipoUsuario> listTU = new ArrayList<>();
		for( int i=0; i<3; i++ ) {
			TipoUsuario tpus = new TipoUsuario();
			tpus.setCoTipoUsuario(i);
			listTU.add(tpus);
		}
		
		List<PermisoSistema> list 	=	repositorio.findByActivoAndCoSistemaAndCoTipoUsuarioIn(id, sis, listTU);
		
		return list.stream().map(element -> Util.getMapperSetting().map(element, PermisoSistemaDTO.class))
				.collect(Collectors.toList());
	}

	@Override
	public PermisoSistemaDTO consultarPermisoSistemaByUser(String u) {
		// TODO Auto-generated method stub
		return null;
	}

}
