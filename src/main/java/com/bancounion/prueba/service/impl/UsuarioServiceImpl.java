package com.bancounion.prueba.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bancounion.DTO.UsuarioDTO;
import com.bancounion.prueba.entities.Sistema;
import com.bancounion.prueba.entities.Usuario;
import com.bancounion.prueba.repo.UsuarioRepository;
import com.bancounion.prueba.service.UsuarioService;
import com.bancounion.prueba.util.Util;
@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepo;
	
	@Override
	public List<Object> listarUsuarios() {
		
		List<Integer> lista = new ArrayList<>();
		for( int i=0; i<3; i++ ) {
			lista.add(i);
		}
		Sistema sis = new Sistema();
		sis.setId(1);
		List<Usuario> list = (List<Usuario>) usuarioRepo.findByPermisoList_ActivoAndPermisoList_CoSistemaIdAndPermisoList_CoTipoUsuarioCoTipoUsuarioIn("S", 1,lista);
		
		return  list.stream().map(element -> Util.getMapperSetting().map(element, UsuarioDTO.class))
				.collect(Collectors.toList());
		}

	@Override
	public Optional<Usuario> consultarUsuario(String u) {
		
		return usuarioRepo.findById(u);
	}

}
