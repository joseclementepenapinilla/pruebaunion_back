package com.bancounion.prueba.service.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancounion.DTO.FacturaForRelatorioDTO;
import com.bancounion.DTO.GraficoGananciasDTO;
import com.bancounion.prueba.entities.Cliente;
import com.bancounion.prueba.entities.Factura;
import com.bancounion.prueba.entities.OrdenServicio;
import com.bancounion.prueba.entities.Usuario;
import com.bancounion.prueba.repo.FacturaRepository;
import com.bancounion.prueba.repo.UsuarioRepository;
import com.bancounion.prueba.service.FacturaService;
import com.bancounion.prueba.util.GraphFacturaRelatorio;
import com.bancounion.prueba.util.GraphFacturaRelatorioDetail;
import com.bancounion.prueba.util.MesesDelAnio;
import com.bancounion.prueba.util.Util;

@Service
public class FacturaServiceImpl implements FacturaService {

	@Autowired
	FacturaRepository repoFact;
	
	@Autowired
	UsuarioRepository userRepo;
	
	@Override
	public List<Factura> getFacturaByCLiente(Cliente cl)  throws FileNotFoundException{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Factura> getFacturaByOs(OrdenServicio o)  throws FileNotFoundException{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getFacturaByUser(String user)  {
		
		List<GraphFacturaRelatorioDetail> listResult = new ArrayList<>();

		GraphFacturaRelatorio	    result 	 	= 	new GraphFacturaRelatorio();
		List<GraphFacturaRelatorio>resultList	=	new ArrayList<>();
		/*TOTALES*/
		Double totalRecetaLiquida	=	(double) 0;
		Double totalComision		=	(double) 0;
		Double totalLucro			=	(double) 0;
		Double totalCostoFijo		=   (double) 0;
		
		Optional<Usuario> usr 		= userRepo.findById(user);		
		List<Object[]> list 		= repoFact.getGeneric(usr.get());		
		for(Object[] obj : list) {
			GraphFacturaRelatorioDetail resultDetail =	new GraphFacturaRelatorioDetail();
			
			resultDetail.setFactura((Factura) obj[0]);
			resultDetail.setValor((Double) obj[1]);

			/*divisor para formatear a decimales para porcentaje*/
			Double divPorc = (double)100;
						
			/*valor de valorTotalImp y comisionCn debe ser formateado a  centimos*/
			Double valorTotalImp 	= (double) resultDetail.getFactura().getTota_imp_inc()/(divPorc);
			Double comisionCn		= (double) resultDetail.getFactura().getComision()/(divPorc);
			
			Double valor 			= (double) resultDetail.getValor() ;			
			/*RECEITA LIQUIDA = VALOR - TOTAL_IMP_INC */
			Double receitaLiq 		= (double) Math.round(valor - (valor*valorTotalImp));
			resultDetail.setRecetaLiquida(receitaLiq);
			totalRecetaLiquida 		+= receitaLiq;
			
			/*COSTO FIJO = BRUT_SALARIO*/
			Double costoFijo = 0.0;
			if( !(resultDetail.getFactura().getOrdenServicio().getUsuario().getSalario() == null) ) {
			costoFijo 		= (double) Math.round(resultDetail.getFactura().getOrdenServicio().getUsuario().getSalario().getBrut_salario());
			resultDetail.setCostoFijo(costoFijo);
			totalCostoFijo += costoFijo;
			}
			
			/*VALOR COMISION =  (VALOR – (VALOR*TOTAL_IMP_INC))*COMISSAO_CN */
			Double comision 		= (double) Math.round((valor -(valor*valorTotalImp)*comisionCn));
			resultDetail.setComision(comision);
			totalComision += comision;
			
			/* LUCRO  (VALOR-TOTAL_IMP_INC) – (Costo fijo + comisión).*/
			//ALERTA como dice el requerimiento
			//Double lucro 		= (valor-valorTotalImp) - (costoFijo + comision);
			//Ccomo dice la logica matematica PREGUNTAR costo fijo es un valor centesimo sedebe multiplicar por un valor para hallarse el porcentaje
			Double lucro 	= (double) (valor-(valor*valorTotalImp)) -(costoFijo + (costoFijo*comision));
			resultDetail.setLucro((double) Math.round(lucro));
			totalLucro		+=  lucro;
			
			listResult.add(resultDetail);
		}
		
		/*set estructura a presentar*/
		result.setListFacturas(listResult);
		result.setTotalComision(totalComision);
		result.setTotalRecetaLiquida(totalRecetaLiquida);
		result.setTotalLucro(totalLucro);
		result.setRelator(usr.get().getNo_usuario());
		result.setTotalCostoFijo(totalCostoFijo);
		
		return Util.getMapperSetting().map(result, FacturaForRelatorioDTO.class);
	}

	@Override
	public Object getFacturaForGraph(String user) {

		Optional<Usuario> 	usr = userRepo.findById(user);		
		List<Object[]> 		list= repoFact.getGenericGraph(usr.get());	
		GraficoGananciasDTO grafGanancia = new GraficoGananciasDTO();
		List<Double> lista = new ArrayList<Double>() ;

		for (MesesDelAnio mes : MesesDelAnio.values()) {
			System.out.println("mes numero"+mes.getNumero());
			lista.add(mes.getNumero()-1, (double)0.0);
		}
		/*divisor para formatear a decimales para porcentaje*/
		Double divPorc = (double)100;
		for(Object[] obj : list) {
			
			GraphFacturaRelatorioDetail resultDetail =	new GraphFacturaRelatorioDetail();
			resultDetail.setFactura((Factura) obj[0]);
			resultDetail.setValor((Double) obj[1]);
			
			/*valor de valorTotalImp y comisionCn debe ser formateado a  centimos*/
			Double valorTotalImp 	= (double) resultDetail.getFactura().getTota_imp_inc()/(divPorc);
			Double valor 			= (double) resultDetail.getValor() ;			
			/*RECEITA LIQUIDA = VALOR - TOTAL_IMP_INC */
			Double receitaLiq 		= (double) Math.round(valor - (valor*valorTotalImp));
			lista.set(Integer.parseInt(obj[2].toString())-1, (double) Math.round( receitaLiq ) );
		}
		grafGanancia.setType("column");
		grafGanancia.setName(user);
		grafGanancia.setData(lista);
		return grafGanancia;
	}

}
