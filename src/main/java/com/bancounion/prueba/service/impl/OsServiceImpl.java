package com.bancounion.prueba.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bancounion.DTO.OrdenServicioDTO;
import com.bancounion.prueba.entities.OrdenServicio;
import com.bancounion.prueba.entities.Usuario;
import com.bancounion.prueba.repo.*;
import com.bancounion.prueba.service.OsService;
import com.bancounion.prueba.util.Util;
@Service
public class OsServiceImpl implements OsService {

	@Autowired
	OsClassRepository repoOs;
	
	@Autowired
	UsuarioRepository repoUsuario;
	
	@Override
	public List<OrdenServicioDTO> getOsByUsuario(String u) {
		Optional<Usuario> usr = repoUsuario.findById(u);
		List<OrdenServicio> list = repoOs.findByUsuario(usr.get());
		
		return  list.stream().map(element -> Util.getMapperSetting().map(element, OrdenServicioDTO.class))
				.collect(Collectors.toList());
	}

}
