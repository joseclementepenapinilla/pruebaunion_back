package com.bancounion.prueba.service;

import java.util.List;
import java.util.Optional;

import com.bancounion.prueba.entities.Usuario;

public interface UsuarioService {

	public List<Object> listarUsuarios();
	
	public Optional<Usuario> consultarUsuario(String u);
	
}
