package com.bancounion.prueba.service;

import java.util.List;

import com.bancounion.DTO.PermisoSistemaDTO;
import com.bancounion.prueba.entities.PermisoSistema;

public interface PermisoSistemaService {
	
	public List<PermisoSistemaDTO> listarUsuariosCondition(String id, int sistema);
	
	public PermisoSistemaDTO consultarPermisoSistemaByUser(String u);
	
}