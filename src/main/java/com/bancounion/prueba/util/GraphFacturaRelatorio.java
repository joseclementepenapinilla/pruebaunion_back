package com.bancounion.prueba.util;

import java.util.List;

public class GraphFacturaRelatorio {

	private List<GraphFacturaRelatorioDetail> listFacturas;

	private Double totalRecetaLiquida;

	private Double totalCostoFijo;

	private Double totalComision;

	private Double totalLucro;

	private String relator;

	public GraphFacturaRelatorio(List<GraphFacturaRelatorioDetail> listFacturas, Double totalRecetaLiquida,
			Double totalCostoFijo, Double totalComision, Double totalLucro, String relator) {
		super();
		this.listFacturas = listFacturas;
		this.totalRecetaLiquida = totalRecetaLiquida;
		this.totalCostoFijo = totalCostoFijo;
		this.totalComision = totalComision;
		this.totalLucro = totalLucro;
		this.relator = relator;
	}

	public Double getTotalCostoFijo() {
		return totalCostoFijo;
	}

	public void setTotalCostoFijo(Double totalCostoFijo) {
		this.totalCostoFijo = totalCostoFijo;
	}

	public String getRelator() {
		return relator;
	}

	public void setRelator(String relator) {
		this.relator = relator;
	}

	public GraphFacturaRelatorio() {
	}

	public List<GraphFacturaRelatorioDetail> getListFacturas() {
		return listFacturas;
	}

	public void setListFacturas(List<GraphFacturaRelatorioDetail> listFacturas) {
		this.listFacturas = listFacturas;
	}

	public Double getTotalRecetaLiquida() {
		return totalRecetaLiquida;
	}

	public void setTotalRecetaLiquida(Double totalRecetaLiquida) {
		this.totalRecetaLiquida = totalRecetaLiquida;
	}

	public Double getTotalComision() {
		return totalComision;
	}

	public void setTotalComision(Double totalComision) {
		this.totalComision = totalComision;
	}

	public Double getTotalLucro() {
		return totalLucro;
	}

	public void setTotalLucro(Double totalLucro) {
		this.totalLucro = totalLucro;
	}

}
