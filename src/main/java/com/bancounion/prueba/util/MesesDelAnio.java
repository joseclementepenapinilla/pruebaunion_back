package com.bancounion.prueba.util;

public enum MesesDelAnio {
    ENERO(1),
    FEBRERO(2),
    MARZO(3),
    ABRIL(4),
    MAYO(5),
    JUNIO(6),
    JULIO(7),
    AGOSTO(8),
    SEPTIEMBRE(9),
    OCTUBRE(10),
    NOVIEMBRE(11),
    DICIEMBRE(12);

    private final int numero;

    MesesDelAnio(int numero) {
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }

    public static int getNumeroDelMes(String nombreDelMes) {
        return MesesDelAnio.valueOf(nombreDelMes.toUpperCase()).getNumero();
    }
}
