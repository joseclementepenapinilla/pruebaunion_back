package com.bancounion.prueba.util;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

public class Util {
	
	public static ModelMapper mapper = new ModelMapper();
	
	public static ModelMapper getMapperSetting() {
		
		ModelMapper map =  new ModelMapper();
		map.getConfiguration().setAmbiguityIgnored(true);
		return map;
	}
	
		
	public static List<Object> responseListError(GenericResponse gr) 
	{
		List<Object> grl = new ArrayList<Object>();
		grl.add(gr);
		return grl;
	}
	
	
	
}
