package com.bancounion.prueba.util;

import com.bancounion.prueba.entities.Factura;

public class GraphFacturaRelatorioDetail {

	private Factura factura;
	
	private Double valor;

	private Double costoFijo;
	
	private Double recetaLiquida;
	
	private Double comision;
	
	private Double lucro;

	public GraphFacturaRelatorioDetail() {
		super();
	}

	public GraphFacturaRelatorioDetail(Factura factura, Double totalImpuestos, Double costoFijo, Double recetaLiquida,
			Double comision, Double lucro) {
		super();
		this.factura = factura;
		this.valor = totalImpuestos;
		this.costoFijo = costoFijo;
		this.recetaLiquida = recetaLiquida;
		this.comision = comision;
		this.lucro = lucro;
	}

	public Double getCostoFijo() {
		return costoFijo;
	}

	public void setCostoFijo(Double costoFijo) {
		this.costoFijo = costoFijo;
	}

	public Double getLucro() {
		return lucro;
	}

	public void setLucro(Double lucro) {
		this.lucro = lucro;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double totalImpuestos) {
		this.valor = totalImpuestos;
	}

	public Double getRecetaLiquida() {
		return recetaLiquida;
	}

	public void setRecetaLiquida(Double recetaLiquida) {
		this.recetaLiquida = recetaLiquida;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}
	
}
