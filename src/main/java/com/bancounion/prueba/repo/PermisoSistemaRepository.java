package com.bancounion.prueba.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancounion.prueba.entities.PermisoSistema;
import com.bancounion.prueba.entities.Sistema;
import com.bancounion.prueba.entities.TipoUsuario;
import com.bancounion.prueba.entities.embedded.PermisoSistemaKey;

@Repository
public interface PermisoSistemaRepository extends CrudRepository<PermisoSistema, PermisoSistemaKey> {
	

	List<PermisoSistema> findByActivoAndCoSistemaAndCoTipoUsuarioIn(String activo, Sistema sistema,List<TipoUsuario> tipoU);
	
	List<PermisoSistema> findByActivoAndCoTipoUsuarioAndCoSistema(String id, TipoUsuario tu,Sistema sistema);
	
	List<PermisoSistema> findByCoTipoUsuario_CoTipoUsuarioIn(List<Integer> tipoU);
	
}
