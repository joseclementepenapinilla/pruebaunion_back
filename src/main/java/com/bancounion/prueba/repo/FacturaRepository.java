package com.bancounion.prueba.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bancounion.prueba.entities.Factura;
import com.bancounion.prueba.entities.OrdenServicio;
import com.bancounion.prueba.entities.Usuario;

@Repository
public interface FacturaRepository extends CrudRepository<Factura, Long> {

	/**
	 * MYSQL 
	 */
	@Query(value = "SELECT os,function('sum',os.valor),function('date_format', os.fecha_emision, '%m') FROM Factura os Where os.ordenServicio.usuario= :us " 
			+"GROUP BY function('date_format', os.fecha_emision, '%m')")
	List<Object[]>  getGeneric(@Param("us") Usuario us);
	
	/**
	 * MYSQL 
	 */
	@Query(value = "SELECT os,function('sum',os.valor),function('date_format', os.fecha_emision, '%c') FROM Factura os Where os.ordenServicio.usuario= :us " 
			+"GROUP BY function('date_format', os.fecha_emision, '%m')")
	List<Object[]>  getGenericGraph(@Param("us") Usuario us);
}
