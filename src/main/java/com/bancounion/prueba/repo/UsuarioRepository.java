package com.bancounion.prueba.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancounion.prueba.entities.Usuario;
@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, String> {

	List<Usuario> findByPermisoList_ActivoAndPermisoList_CoSistemaIdAndPermisoList_CoTipoUsuarioCoTipoUsuarioIn(String activo, Integer cs,List<Integer> list );
}
