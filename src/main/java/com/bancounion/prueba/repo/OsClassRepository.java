package com.bancounion.prueba.repo;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bancounion.prueba.entities.OrdenServicio;
import com.bancounion.prueba.entities.Usuario;

@Repository
public interface OsClassRepository extends CrudRepository<OrdenServicio, Long> {

	List<OrdenServicio> findByUsuario(Usuario us);

}
