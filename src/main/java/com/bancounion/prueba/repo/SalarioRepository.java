package com.bancounion.prueba.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancounion.prueba.entities.Salario;
import com.bancounion.prueba.entities.embedded.SalarioKey;

@Repository
public interface SalarioRepository extends CrudRepository<Salario, SalarioKey> {

}
