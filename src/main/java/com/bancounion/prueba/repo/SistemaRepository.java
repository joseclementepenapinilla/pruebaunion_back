package com.bancounion.prueba.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bancounion.prueba.entities.Sistema;
@Repository
public interface SistemaRepository extends CrudRepository<Sistema, Long> {

}
