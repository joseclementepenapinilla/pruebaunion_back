package com.bancounion.prueba.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancounion.DTO.PermisoSistemaDTO;
import com.bancounion.prueba.service.PermisoSistemaService;
import com.bancounion.prueba.service.UsuarioService;
import com.bancounion.prueba.util.GenericResponse;
import com.bancounion.prueba.util.Util;

import jakarta.persistence.EntityNotFoundException;

@CrossOrigin
@RestController
@RequestMapping("main")
public class MainController {

	@Autowired
	private PermisoSistemaService servicePermisoSistema;

	@Autowired
	private UsuarioService serviceUsuario;

	@GetMapping("/consultor")
	  public List<Object> getConsultor(){
		try {
		  return serviceUsuario.listarUsuarios();
		}catch(EntityNotFoundException e) {			
			return Util.responseListError(new GenericResponse("0",e.getMessage()));
		}
	}

	@GetMapping("/consultor/permisos")
	public List<PermisoSistemaDTO> getConsultorPermisos() {
		try {
			List<PermisoSistemaDTO> lista=new ArrayList<>();
			
			return  servicePermisoSistema.listarUsuariosCondition("S",1);
		} catch (EntityNotFoundException e) {
			//return Util.responseListError(new GenericResponse("0", e.getMessage()));
		return null;
		}
	}
}
