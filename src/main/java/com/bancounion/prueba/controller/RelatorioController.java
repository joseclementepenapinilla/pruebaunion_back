package com.bancounion.prueba.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.bancounion.DTO.FacturaDTO;
import com.bancounion.DTO.OrdenServicioDTO;
import com.bancounion.DTO.UsuarioDTO;
import com.bancounion.DTO.request.RequestGeneric;
import com.bancounion.prueba.entities.Usuario;
import com.bancounion.prueba.service.FacturaService;
import com.bancounion.prueba.service.OsService;
import com.bancounion.prueba.service.UsuarioService;

@CrossOrigin
@RestController
@RequestMapping("relatorio")
public class RelatorioController {

	@Autowired
	FacturaService serviceFactura;
	
	@Autowired
	OsService servicioOrden;
	
	@Autowired
	UsuarioService servicioUsuario;
	
    @GetMapping("/ganancias/{user}")
	public Object getInfo(@PathVariable("user") String request) {
    	//try {
    	Optional<Usuario> usr ;
    	usr = servicioUsuario.consultarUsuario(request);
    	return serviceFactura.getFacturaByUser(usr.get().getCoUsuario());
    	
    }
		
    @GetMapping("/ganancias/grafica/{user}")
    public Object getInfoGrafica(@PathVariable("user") String request) {
    	Optional<Usuario> usr ;
    	usr = servicioUsuario.consultarUsuario(request);
    	return serviceFactura.getFacturaForGraph(usr.get().getCoUsuario());  
    	
    }
    
    @PostMapping("/gana")
	public UsuarioDTO getInfo() {
    //public List<OrdenServicioDTO> getInfo() {
    	UsuarioDTO usu = new UsuarioDTO();
    	return usu;
    	//return servicioOrden.getOsByUsuario(u);
    }
	
}
