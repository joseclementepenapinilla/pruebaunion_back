package com.bancounion.DTO;

import java.util.Date;

public class SistemaDTO {
	
	private int id;
	
	private Long id_cliente;
	
	private String usuario;
	
	private int id_arquitectura;
	
	private String no_sistema;
	
	private String resumen_sistema;
	
	private String caracteristica;
	
	private String requisito;
	
	private String directoria;
	
	private String telefono;
	
	private String  telefono_solic;
	
	private String usuario_solicitud;
	
	private Date fecha_solicitud;
	
	private Date fecha_entrega;
	
	private String correo_electronico;

	public SistemaDTO() {
		super();
	}

	public SistemaDTO(int id, Long id_cliente, String usuario, int id_arquitectura, String no_sistema,
			String resumen_sistema, String caracteristica, String requisito, String directoria, String telefono,
			String telefono_solic, String usuario_solicitud, Date fecha_solicitud, Date fecha_entrega,
			String correo_electronico) {
		super();
		this.id = id;
		this.id_cliente = id_cliente;
		this.usuario = usuario;
		this.id_arquitectura = id_arquitectura;
		this.no_sistema = no_sistema;
		this.resumen_sistema = resumen_sistema;
		this.caracteristica = caracteristica;
		this.requisito = requisito;
		this.directoria = directoria;
		this.telefono = telefono;
		this.telefono_solic = telefono_solic;
		this.usuario_solicitud = usuario_solicitud;
		this.fecha_solicitud = fecha_solicitud;
		this.fecha_entrega = fecha_entrega;
		this.correo_electronico = correo_electronico;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Long getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(Long id_cliente) {
		this.id_cliente = id_cliente;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public int getId_arquitectura() {
		return id_arquitectura;
	}

	public void setId_arquitectura(int id_arquitectura) {
		this.id_arquitectura = id_arquitectura;
	}

	public String getNo_sistema() {
		return no_sistema;
	}

	public void setNo_sistema(String no_sistema) {
		this.no_sistema = no_sistema;
	}

	public String getResumen_sistema() {
		return resumen_sistema;
	}

	public void setResumen_sistema(String resumen_sistema) {
		this.resumen_sistema = resumen_sistema;
	}

	public String getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}

	public String getRequisito() {
		return requisito;
	}

	public void setRequisito(String requisito) {
		this.requisito = requisito;
	}

	public String getDirectoria() {
		return directoria;
	}

	public void setDirectoria(String directoria) {
		this.directoria = directoria;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono_solic() {
		return telefono_solic;
	}

	public void setTelefono_solic(String telefono_solic) {
		this.telefono_solic = telefono_solic;
	}

	public String getUsuario_solicitud() {
		return usuario_solicitud;
	}

	public void setUsuario_solicitud(String usuario_solicitud) {
		this.usuario_solicitud = usuario_solicitud;
	}

	public Date getFecha_solicitud() {
		return fecha_solicitud;
	}

	public void setFecha_solicitud(Date fecha_solicitud) {
		this.fecha_solicitud = fecha_solicitud;
	}

	public Date getFecha_entrega() {
		return fecha_entrega;
	}

	public void setFecha_entrega(Date fecha_entrega) {
		this.fecha_entrega = fecha_entrega;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

}
