package com.bancounion.DTO;

import java.util.Date;

import lombok.*;

@Getter @Setter
public class SalarioKeyDTO {

	private String usuario;

	private Date fecha_modificacion;

	public SalarioKeyDTO() {}
	public SalarioKeyDTO(String usuario, Date fecha_modificacion) {
		super();
		this.usuario = usuario;
		this.fecha_modificacion = fecha_modificacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

}
