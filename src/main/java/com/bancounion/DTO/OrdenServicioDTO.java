package com.bancounion.DTO;

import java.util.Date;

public class OrdenServicioDTO {

	private Integer co_os;
	
	private Integer nu_os;

	private Integer co_sistema;
	
	private UsuarioDTO usuario;
		
	private Integer co_arquitectura;
	
	private String ds_os;
	
	private String caracteristica;
	
	private String requisito;
	
	private Date fecha_inicio;
	
	private Date fecha_fin;
	
	private Integer status;
	
	private String directorio;
	
	private Date fecha_sol;
	
	private String nu_tel_sol;
	
	private String ddd_tel_sol;
	
	private String nu_tel_sol_dos;
	
	private String ddd_tel_sol_dos;
	
	private String usuario_sol;
	
	private Date fecha_imp;
	
	private Date fecha_garantia;
	
	private String correo_electronico;
	
	private String prospect_rel;

	public OrdenServicioDTO() {}

	public OrdenServicioDTO(Integer co_os, Integer nu_os, Integer idSistema, UsuarioDTO coUsuario, 
			Integer co_arquitectura, String ds_os, String caracteristica, String requisito, Date fecha_inicio,
			Date fecha_fin, Integer status, String directorio, Date fecha_sol, String nu_tel_sol, String ddd_tel_sol,
			String nu_tel_sol_dos, String ddd_tel_sol_dos, String usuario_sol, Date fecha_imp, Date fecha_garantia,
			String correo_electronico, String prospect_rel) {
		super();
		this.co_os = co_os;
		this.nu_os = nu_os;
		this.co_sistema = idSistema;
		this.usuario = coUsuario;
		this.co_arquitectura = co_arquitectura;
		this.ds_os = ds_os;
		this.caracteristica = caracteristica;
		this.requisito = requisito;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.status = status;
		this.directorio = directorio;
		this.fecha_sol = fecha_sol;
		this.nu_tel_sol = nu_tel_sol;
		this.ddd_tel_sol = ddd_tel_sol;
		this.nu_tel_sol_dos = nu_tel_sol_dos;
		this.ddd_tel_sol_dos = ddd_tel_sol_dos;
		this.usuario_sol = usuario_sol;
		this.fecha_imp = fecha_imp;
		this.fecha_garantia = fecha_garantia;
		this.correo_electronico = correo_electronico;
		this.prospect_rel = prospect_rel;
	}


	public Integer getCo_os() {
		return co_os;
	}


	public void setCo_os(Integer co_os) {
		this.co_os = co_os;
	}


	public Integer getNu_os() {
		return nu_os;
	}


	public void setNu_os(Integer nu_os) {
		this.nu_os = nu_os;
	}


	public Integer getCo_sistema() {
		return co_sistema;
	}


	public void setCo_sistema(Integer co_sistema) {
		this.co_sistema = co_sistema;
	}


	public UsuarioDTO getUsuario() {
		return usuario;
	}


	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}


	public Integer getCo_arquitectura() {
		return co_arquitectura;
	}


	public void setCo_arquitectura(Integer co_arquitectura) {
		this.co_arquitectura = co_arquitectura;
	}


	public String getDs_os() {
		return ds_os;
	}


	public void setDs_os(String ds_os) {
		this.ds_os = ds_os;
	}


	public String getCaracteristica() {
		return caracteristica;
	}


	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}


	public String getRequisito() {
		return requisito;
	}


	public void setRequisito(String requisito) {
		this.requisito = requisito;
	}


	public Date getFecha_inicio() {
		return fecha_inicio;
	}


	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}


	public Date getFecha_fin() {
		return fecha_fin;
	}


	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getDirectorio() {
		return directorio;
	}


	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}


	public Date getFecha_sol() {
		return fecha_sol;
	}


	public void setFecha_sol(Date fecha_sol) {
		this.fecha_sol = fecha_sol;
	}


	public String getNu_tel_sol() {
		return nu_tel_sol;
	}


	public void setNu_tel_sol(String nu_tel_sol) {
		this.nu_tel_sol = nu_tel_sol;
	}


	public String getDdd_tel_sol() {
		return ddd_tel_sol;
	}


	public void setDdd_tel_sol(String ddd_tel_sol) {
		this.ddd_tel_sol = ddd_tel_sol;
	}


	public String getNu_tel_sol_dos() {
		return nu_tel_sol_dos;
	}


	public void setNu_tel_sol_dos(String nu_tel_sol_dos) {
		this.nu_tel_sol_dos = nu_tel_sol_dos;
	}


	public String getDdd_tel_sol_dos() {
		return ddd_tel_sol_dos;
	}


	public void setDdd_tel_sol_dos(String ddd_tel_sol_dos) {
		this.ddd_tel_sol_dos = ddd_tel_sol_dos;
	}


	public String getUsuario_sol() {
		return usuario_sol;
	}


	public void setUsuario_sol(String usuario_sol) {
		this.usuario_sol = usuario_sol;
	}


	public Date getFecha_imp() {
		return fecha_imp;
	}


	public void setFecha_imp(Date fecha_imp) {
		this.fecha_imp = fecha_imp;
	}


	public Date getFecha_garantia() {
		return fecha_garantia;
	}


	public void setFecha_garantia(Date fecha_garantia) {
		this.fecha_garantia = fecha_garantia;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public String getProspect_rel() {
		return prospect_rel;
	}

	public void setProspect_rel(String prospect_rel) {
		this.prospect_rel = prospect_rel;
	}

}
