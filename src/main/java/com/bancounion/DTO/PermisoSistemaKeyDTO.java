package com.bancounion.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PermisoSistemaKeyDTO {
	
	private String coUsuario;

	private Long coTipoUsuario;

	private Long coSistema;
	
	public PermisoSistemaKeyDTO(){}

	public PermisoSistemaKeyDTO(String coUsuario, Long coTipoUsuario, Long coSistema) {
		super();
		this.coUsuario = coUsuario;
		this.coTipoUsuario = coTipoUsuario;
		this.coSistema = coSistema;
	}

	public String getCoUsuario() {
		return coUsuario;
	}

	public void setCoUsuario(String coUsuario) {
		this.coUsuario = coUsuario;
	}

	public Long getCoTipoUsuario() {
		return coTipoUsuario;
	}

	public void setCoTipoUsuario(Long coTipoUsuario) {
		this.coTipoUsuario = coTipoUsuario;
	}

	public Long getCoSistema() {
		return coSistema;
	}

	public void setCoSistema(Long coSistema) {
		this.coSistema = coSistema;
	}
	
}
