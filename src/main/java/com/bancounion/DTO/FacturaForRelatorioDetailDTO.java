package com.bancounion.DTO;

public class FacturaForRelatorioDetailDTO {

	private FacturaDTO factura;
	
	private Double valor;

	private Double costoFijo;
	
	private Double recetaLiquida;

	private Double comision;
	
	private Double lucro;
	
	public FacturaForRelatorioDetailDTO() {
		super();
	}

	public FacturaForRelatorioDetailDTO(FacturaDTO factura, Double totalImpuestos, Double costoFijo, Double recetaLiquida,
			Double comision, Double lucro) {
		super();
		this.factura = factura;
		this.valor = totalImpuestos;
		this.costoFijo = costoFijo;
		this.recetaLiquida = recetaLiquida;
		this.comision = comision;
		this.lucro = lucro;
	}


	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getCostoFijo() {
		return costoFijo;
	}

	public void setCostoFijo(Double costoFijo) {
		this.costoFijo = costoFijo;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}

	public Double getLucro() {
		return lucro;
	}

	public void setLucro(Double lucro) {
		this.lucro = lucro;
	}

	public Double getRecetaLiquida() {
		return recetaLiquida;
	}

	public void setRecetaLiquida(Double recetaLiquida) {
		this.recetaLiquida = recetaLiquida;
	}

	public FacturaDTO getFactura() {
		return factura;
	}

	public void setFactura(FacturaDTO factura) {
		this.factura = factura;
	}

	public Double getTotalImpuestos() {
		return valor;
	}

	public void setTotalImpuestos(Double totalImpuestos) {
		this.valor = totalImpuestos;
	}
	
}
