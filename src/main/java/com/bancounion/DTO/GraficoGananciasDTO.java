package com.bancounion.DTO;

import java.util.List;

public class GraficoGananciasDTO {
	
	private String type;
	
	private String name;
	
	private List<Double> data;

	public GraficoGananciasDTO() {
		super();
	}

	public GraficoGananciasDTO(String type, String name, List<Double> data) {
		super();
		this.type = type;
		this.name = name;
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Double> getData() {
		return data;
	}

	public void setData(List<Double> data) {
		this.data = data;
	}
	
}
