package com.bancounion.DTO;


import java.util.Date;

import lombok.*;

@Getter @Setter
public class SalarioDTO {

	private String usuario;

	private Date dt_alteracao;
	
	private Float brut_salario;
	
	private Float liq_salario;

	public SalarioDTO() {}
	
	public SalarioDTO(String usuario, Date fecha_modificacion, Float  salarioBruto, Float  salarioLiquido) {
		super();
		this.usuario = usuario;
		this.dt_alteracao = fecha_modificacion;
		this.brut_salario = salarioBruto;
		this.liq_salario = salarioLiquido;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public Date getDt_alteracao() {
		return dt_alteracao;
	}

	public void setDt_alteracao(Date dt_alteracao) {
		this.dt_alteracao = dt_alteracao;
	}

	public Float getBrut_salario() {
		return brut_salario;
	}

	public void setBrut_salario(Float brut_salario) {
		this.brut_salario = (float) 10000;
	}

	public Float getLiq_salario() {
		return liq_salario;
	}

	public void setLiq_salario(Float liq_salario) {
		this.liq_salario = liq_salario;
	}

}
