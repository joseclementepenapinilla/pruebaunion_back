package com.bancounion.DTO.request;

public class RequestGeneric {
	
	private String data;
	private String codeData;
	public RequestGeneric(String data, String codeData) {
		super();
		this.data = data;
		this.codeData = codeData;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public String getCodeData() {
		return codeData;
	}
	
	public void setCodeData(String codeData) {
		this.codeData = codeData;
	}
	
}
