package com.bancounion.DTO;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
public class PermisoSistemaDTO {
	
	private UsuarioDTO coUsuario;

	private Integer coTipoUsuario;

	private SistemaDTO coSistema;
	
	private String activo;
	
	private String coUsuarioActualizado;
	
	private Date fechaActualizacion;

	public PermisoSistemaDTO() {}

	public PermisoSistemaDTO(UsuarioDTO  co_usuario, Integer coTipoUsuario, SistemaDTO coSistema, String activo,
			String coUsuarioActualizado, Date fechaActualizacion) {
		super();
		this.coUsuario = co_usuario;
		this.coTipoUsuario = coTipoUsuario;
		this.coSistema = coSistema;
		this.activo = activo;
		this.coUsuarioActualizado = coUsuarioActualizado;
		this.fechaActualizacion = fechaActualizacion;
	}


	public UsuarioDTO  getCoUsuario() {
		return coUsuario;
	}

	public void setCoUsuario(UsuarioDTO coUsuario) {
		this.coUsuario = coUsuario;
	}

	public Integer getCoTipoUsuario() {
		return coTipoUsuario;
	}

	public void setCoTipoUsuario(Integer coTipoUsuario) {
		this.coTipoUsuario = coTipoUsuario;
	}

	public SistemaDTO getCoSistema() {
		return coSistema;
	}

	public void setCoSistema(SistemaDTO coSistema) {
		this.coSistema = coSistema;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getCoUsuarioActualizado() {
		return coUsuarioActualizado;
	}

	public void setCoUsuarioActualizado(String coUsuarioActualizado) {
		this.coUsuarioActualizado = coUsuarioActualizado;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}


}
