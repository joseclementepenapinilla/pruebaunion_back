package com.bancounion.DTO;

import java.util.Date;

public class FacturaDTO {

	private Integer factura;
	
	private Integer cliente;
	
	private SistemaDTO sistema;

	private Integer co_sistema;
	
	private OrdenServicioDTO ordenServicio;
	
	private Integer num_nf;
	
	private float total;
	
	private float valor;
		
	private Date fecha_emision;
	
	private String corpo_nf;
	
	private float comision;
	
	private float tota_imp_inc;

	public FacturaDTO() {}

	public FacturaDTO(Integer factura, Integer cliente, SistemaDTO sistema, Integer co_sistema, OrdenServicioDTO co_us,
			Integer num_nf, float total, float valor, Date fecha_emision, String corpo_nf, float comision,
			float tota_imp_inc) {
		super();
		this.factura = factura;
		this.cliente = cliente;
		this.sistema = sistema;
		this.co_sistema = co_sistema;
		this.ordenServicio = co_us;
		this.num_nf = num_nf;
		this.total = total;
		this.valor = valor;
		this.fecha_emision = fecha_emision;
		this.corpo_nf = corpo_nf;
		this.comision = comision;
		this.tota_imp_inc = tota_imp_inc;
	}

	public Integer getCo_sistema() {
		return co_sistema;
	}

	public void setCo_sistema(Integer co_sistema) {
		this.co_sistema = co_sistema;
	}

	public Integer getFactura() {
		return factura;
	}
	public void setFactura(Integer factura) {
		this.factura = factura;
	}
	public Integer getCliente() {
		return cliente;
	}
	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}
	public SistemaDTO getSistema() {
		return sistema;
	}
	public void setSistema(SistemaDTO sistema) {
		this.sistema = sistema;
	}
	
	public OrdenServicioDTO getOrdenServicio() {
		return ordenServicio;
	}

	public void setOrdenServicio(OrdenServicioDTO ordenServicio) {
		this.ordenServicio = ordenServicio;
	}

	public Integer getNum_nf() {
		return num_nf;
	}
	public void setNum_nf(Integer num_nf) {
		this.num_nf = num_nf;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public Date getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(Date fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	public String getCorpo_nf() {
		return corpo_nf;
	}
	public void setCorpo_nf(String corpo_nf) {
		this.corpo_nf = corpo_nf;
	}
	public float getComision() {
		return comision;
	}
	public void setComision(float comision) {
		this.comision = comision;
	}
	public float getTota_imp_inc() {
		return tota_imp_inc;
	}
	public void setTota_imp_inc(float tota_imp_inc) {
		this.tota_imp_inc = tota_imp_inc;
	}
	
}
